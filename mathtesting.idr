pascalrow_main :: Int -> Vect
pascalrow_main (n,as)
  | n == 0 = (0,as)
  | otherwise = pascalrow_main (n-1,pascal_op (0:as))

-- given row N of pascals triangle this outputs row N+1 (needs  a zero in front of the list)
pascal_op :: Vect -> Vect
pascal_op (a:as)
  | as == [] || as== [0] = [a]
  | otherwise = [a + (head as)] ++ (pascal_op as)
